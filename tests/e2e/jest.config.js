'use strict'

// jest.config.js
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js',
    '!<rootDir>/node_modules/'
  ],
  coverageDirectory: '<rootDir>/tests/coverage/e2e',
  coveragePathIgnorePatterns: [ // shinobi-tensorflow plugin code is from Shinobi, Moe Allam
    '<rootDir>/src/ObjectDetectors.js',
    '<rootDir>/src/shinobi-tensorflow.js'
  ],
  globalSetup: '<rootDir>/tests/e2e/setup/globalSetup.js',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '<rootDir>/tests/coverage/e2e'
      }
    ]
  ],
  rootDir: `${__dirname}/../../`,
  testEnvironment: '<rootDir>/tests/e2e/environment/testEnvironment.js',
  modulePathIgnorePatterns: [
    '<rootDir>/tests/filter.test.js',
    '<rootDir>/tests/mqttClient.test.js',
    '<rootDir>/tests/notify.test.js',
    '<rootDir>/tests/process.test.js',
    '<rootDir>/tests/timer.test.js',
    '<rootDir>/tests/validation.test.js'
  ],
  verbose: true
}
