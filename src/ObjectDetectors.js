
/**
 * Returns an ObjectDetectors instance that provides tensorflow predictions for a given
 * input image.
 *
 * Remarks: Dynamically loads the tensorflow-cpu | tensorflow-gpu library based on
 * plugin config tfsjBuild
 */
module.exports = function (config) {
  var tfjsSuffix = ''
  switch (config.tfjsBuild) {
    case 'gpu':
      tfjsSuffix = '-gpu'
      break
    case 'cpu':
      break
    default:
      try {
        require('@tensorflow/tfjs-node-gpu')
        tfjsSuffix = '-gpu'
      } catch (err) {
        console.error(`Failed loading default @tensorflow/tfjs-node-gpu => ${err}`)
      }
      break
  }

  console.log(`TensorFlow-WithFiltering-And-MQTT plugin loading @tensorflow/tfjs-node${tfjsSuffix} module`)
  var tf = require(`@tensorflow/tfjs-node${tfjsSuffix}`)

  console.log('TensorFlow-WithFiltering-And-MQTT plugin loading @tensorflow-models/coco-ssd module')
  const cocossd = require('@tensorflow-models/coco-ssd')

  async function loadCocoSsdModal () {
    const modal = await cocossd.load({
      base: config.cocoBase || 'lite_mobilenet_v2'
    })
    return modal
  }

  function getTensor3dObject (numOfChannels, imageArray) {
    const tensor3d = tf.node.decodeJpeg(imageArray, numOfChannels)

    return tensor3d
  }

  var loadCocoSsdModel = {
    detect: function () {
      return { data: [] }
    }
  }

  async function init () {
    console.log('TensorFlow-WithFiltering-And-MQTT plugin initialising cocoSsd data')
    loadCocoSsdModel = await loadCocoSsdModal()
  }

  init()

  return class ObjectDetectors {
    constructor (image, type) {
      this.startTime = new Date()
      this.inputImage = image
      this.type = type
    }

    async process () {
      const tensor3D = getTensor3dObject(3, (this.inputImage))
      const predictions = await loadCocoSsdModel.detect(tensor3D)

      tensor3D.dispose()

      return {
        data: predictions,
        type: this.type,
        time: new Date() - this.startTime
      }
    }
  }
}
