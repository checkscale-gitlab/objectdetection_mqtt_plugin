'use strict'

const pino = require('pino')
const log = pino({ name: 'TensorFlow-WithFiltering-And-MQTT', level: 'debug' }, pino.destination(1))

module.exports = {
  parentLogger: log
}
