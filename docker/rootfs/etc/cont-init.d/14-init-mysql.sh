#!/usr/bin/with-contenv bash
# ==============================================================================
# Ensures the database structure is loaded into MySQL
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

declare host
declare username
declare password
declare database
declare port
declare numberOfTables

    
bashio::log.info "Configuring mysql database..."

host=$(bashio::config 'db.host') 
username=$(bashio::config 'db.user')
password=$(bashio::config 'db.password')
database=$(bashio::config 'db.database')
port=$(bashio::config 'db.port')

bashio::log.info "mysql host::${host} : username::${username} : database::${database} : port::${port}"
bashio::log.info "Pinging database server..."

until nc -z -v -w30 ${host} ${port}
do
  echo "Sleeping for 5 seconds before retrying..."
  sleep 5
done

bashio::log.info "Database server is online..."

numberOfTables=$(
    mysql -h "${host}" -u "${username}" -p"${password}" -P "${port}" \
        "${database}" -N -B -e "show tables;" | wc -l
)

if [[ "${numberOfTables}" -eq 0 ]]; then
    bashio::log.info "Creating tables in database..."
    mysql -h "${host}" -u "${username}" -p"${password}" -P "${port}" \
        "${database}" < "/opt/shinobi/sql/tables.sql" ||
            bashio::die "Error while importing database table structure!"
fi
