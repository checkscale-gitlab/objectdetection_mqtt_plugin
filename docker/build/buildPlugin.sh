#!/usr/bin/with-contenv bash
# ==============================================================================
# This file builds the objectdetect plugin
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

bashio::log.info 'Building objectdetection plugin...'

if [ "${BUILD_GPU}" -eq 1 ]; then
    bashio::log.info 'Building dependencies with GPU support'
    yarn install --audit;
else
    bashio::log.info 'Building dependencies without GPU support'
    yarn install --audit --ignore-optional;
fi;

case "${BUILD_ARCH}" in
    core2) bashio::log.info 'Removing default tensorflow lib and headers'
    rm -rf node_modules/@tensorflow/tfjs-node/deps/lib
    rm -rf node_modules/@tensorflow/tfjs-node/deps/include
    bashio::log.info "Downloading and extracting Intel Core2 compatible tensorflow library into /build/plugin/node_modules/@tensorflow/tfjs-node/deps/"
    curl -L -s "https://www.dropbox.com/s/lcgtack8tisfl9a/libtensorflow-1.15.3.tar.gz\?dl\=1" | tar --strip-components=1 -zxvf - -C /build/plugin/node_modules/@tensorflow/tfjs-node/deps/
    ls "/build/plugin/node_modules/@tensorflow/tfjs-node/deps/"
    ;;
    *) bashio::log.info 'Using default tensorflow library for build'
    ;;
esac

bashio::log.info 'Saving build output to build.json'
cat << EOF > build.json
{
    "arch": "${BUILD_ARCH}",
    "gpu": "${BUILD_GPU}"
}
EOF
