# Docker Architecture

A docker-compose stack has been provided within the docker directory. This 
offers the following benefits with respect to development:

- Replicable development / release environment across machine architectures.
  Allows new developers to quickly build the docker-compose stack for 
  performing the installation, allowing more focus on development work. 
- A consistent environment that facilitates replicability across CI Pipelines.
- Useful for conducting Continuous Integration (CI) build and tests. Feature branch 
  commits, trigger a build within the docker-compose stack.
  Unit/integration tests can then be run before push and merge into the main 
  trunk. If any tests fail then the merge of the feature branch into the main
  trunk will fail, ensuring the main trunk is always clean. This is possible in
  GitLab and many other repositories.
- Developers can test their code in a clean fresh environment quickly before 
  pushing changes into repository.

The docker architecture has the following containers provided by docker/docker-compose.yml
and docker/docker-compose.dev/yml files:

- **mqtt**: MQTT broker. Exposes ports 1883, 8883 and 9001
- **mysql**: Database engine instance for Shinobi. Exposes port 3306
- **nginx**: Reverse proxy for Shinobi. Exposes ports 8081 and 4443
- **shinobi**: Shinobi installation containing this plugin operating on internal port 8080

The docker build process consists of two stages:
1. Install node dependencies for shinobi and the plugin into a temporary build container
2. Install Linux package dependencies required for Shinobi and copy across the node modules
   into the final build image

This has the benefit that once the initial node dependencies have been downloaded and 
installed, subsequent builds triggered by source code changes are much quicker. There is no 
need to rebuild Shinobi and the plugin dependencies each time since they are cached.

Additional a docker compose environment has also been provided for running on CI servers:
docker/docker-compose.yml and docker/docker-compose.ci.yml. This provides the following
services:

- **mqtt**: MQTT broker. Exposes ports 1883, 8883 and 9001
- **plugin**: Plugin node_modules installed with local _src_ and _test_ folders mounted

## Getting Started

The Reverse proxy and MQTT broker operate within a TLS connection. It is the developer's 
responsibility to setup these certificates for their local test development environment.

Create the following certificates for localhost in *docker/certs/nginx*:

- server.crt
- server.key

Create the following signed localhost certificates in *docker/certs/mosquitto*:

- localCA.crt 
- server.crt
- server.key

Issue the following commands to build and start the docker-compose
environment:

``` bash
cd docker

# build image containing the plugin installed on shinobi
# rebuild only when package dependencies change
make

# start the docker-compose environment as a virtual environment on local machine
# mount src and tests so that image does not need building due to source code
# or test changes.
make up_dev
```

In a browser visit https://localhost:4443 to display the Shinobi login
page.


## Configuration

Configuration is achieved via environment variables and configuration files.
A *.env.sample* file documents the configuration settings for the containers 
within the docker-compose stack. Make a copy of this file within the *docker*
folder and name it *.env*. The .env file is local to the developers
environment, i.e. it is not committed into source control.

``` bash
# 
# Mysql Database
# Ensure changes are reflected in docker/volumes/shinobi/conf.json
#
DB_DATABASE=ccio
DB_ROOT_PASSWORD=rootpass
DB_USER=majesticflame
DB_PASSWORD=dbpass

#
# MQTT Broker
# Ensure changes are reflected in docker/volumes/shinobi/plugins/objectdetection/conf.json
#
MQTT_USER=user
MQTT_PASSWORD=pass

#
# Plugin build configuration
# Changes require rebuilding image
#

# 
# CPU architecture for tensorflow
# none - use the default core2 architecture
# core2 - use libtensorflow precompiled for Intel Core2 CPUs
# 
# At build time, the appropriate Tensorflow library will be installed
# for the required architecture.
BUILD_ARCH=none

# 
# Allow offloading Tensroflow detection processing to GPU (1) or 
# CPU (0)
#
BUILD_GPU=0
```

The *docker/volumes* folder is not committed into source control and stores:
- Shinobi database
- Configuration files for shinobi and this plugin
- Shinobi videos 
- Shinobi streams

Volumes are automatically created when the docker-compose environment is
first started and persists between runs. 


## Makefile

A makefile exists in the root of the repository. The document header for the
Makefile provides a summary of make rules available.
